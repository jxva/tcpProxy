
#ifndef PRIVATE_LOG_H_
#define PRIVATE_LOG_H_

#define LOG_EMERG       0       /* system is unusable */
#define LOG_ALERT       1       /* action must be taken immediately */
#define LOG_CRIT        2       /* critical conditions */
#define LOG_ERR         3       /* error conditions */
#define LOG_WARNING     4       /* warning conditions */
#define LOG_NOTICE      5       /* normal but significant condition */
#define LOG_INFO        6       /* informational */
#define LOG_DEBUG       7       /* debug-level messages */

#define DBG_DEBUG_ON        "./print_debug_on"
#define DBG_DUMP_ON        "./print_dump_on"

#define DBG_PRINTF(fmt, ...) do {printf("\n");printf("[%s][%s,%s,%d]: "fmt, sys_GetTimeStr(),__FILE__, __FUNCTION__, __LINE__ , ##__VA_ARGS__); printf("\n");} while(0)
#define DBG_ASSERT(bool, fmt, ...) do { if(!(bool)) { printf("\nFAIL [%s][%s,%s,%d]: "fmt, sys_GetTimeStr(),__FILE__, __FUNCTION__, __LINE__ , ##__VA_ARGS__); printf("\r\n"); } } while(0)

void sys_syslog(int priority, const char *fmt, ...);
char * sys_GetTimeStr();
extern UINT32 LogIsNeedPrint(int Level);

#define CW_SendDebug(fmt, ...) do \
{\
    if(TRUE ==LogIsNeedPrint(LOG_DEBUG))\
    {  \
        sys_syslog(LOG_DEBUG, "[%s][%s,%s,%d]: "fmt, sys_GetTimeStr(),(char *)__FILE__, __FUNCTION__, __LINE__ , ##__VA_ARGS__);\
        DBG_PRINTF(fmt, ##__VA_ARGS__);\
    }  \
}while(0)
#define CW_SendInfo(fmt, ...) do\
{\
    sys_syslog(LOG_INFO, "[%s][%s,%s,%d]: "fmt, sys_GetTimeStr(), (char *)__FILE__, __FUNCTION__, __LINE__ , ##__VA_ARGS__);\
    DBG_PRINTF(fmt, ##__VA_ARGS__);\
}while(0)

#define CW_SendErr(fmt, ...) do\
{\
    sys_syslog(LOG_ERR, "[%s][%s,%s,%d]: "fmt, sys_GetTimeStr(), (char *)__FILE__, __FUNCTION__, __LINE__ , ##__VA_ARGS__);\
    DBG_PRINTF(fmt, ##__VA_ARGS__);\
}while(0)

#define CW_SendLog(level, fmt, ...) do\
{\
    sys_syslog(level, "[%s][%s,%s,%d]: "fmt, sys_GetTimeStr(),(char *)__FILE__, __FUNCTION__, __LINE__ , ##__VA_ARGS__);\
    DBG_PRINTF(fmt, ##__VA_ARGS__);\
}while(0)

#define CW_SendDump(fmt, ...) do\
{\
    if(TRUE ==DumpLogIsNeedPrint())\
    { \
        sys_syslog(LOG_ERR, fmt, ##__VA_ARGS__);\
        printf(fmt, ##__VA_ARGS__);\
    }  \
}while(0)

#endif /* TIMER_H_ */
