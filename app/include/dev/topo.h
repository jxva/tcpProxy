/*-------------------------------------------------------------------------
    topo.h
-------------------------------------------------------------------------*/

#ifndef TOPO_H_
#define TOPO_H_

#define	TOPO_MAX_USER_ID   21  /*身份识别码的最大长度*/


#define	TOPO_MAX_USER   65535  /*hash长度*/
#define TOPO_MAX_USER_NAME 64
#define TOPO_MAX_USER_PASSWD 64

/* time out */
#define TOPO_TIMEOUT_GRIT 60

#define TOPO_TIMEOUT_INIT 300
#define TOPO_TIMEOUT_AUTH 300
#define TOPO_TIMEOUT_CONFIG 300
#define TOPO_TIMEOUT_ONLINE 1500
#define TOPO_TIMEOUT_OFFLINE 300

#define TOPO_GPS_DBA "gps.db"

typedef enum
{
    TOPO_STATUS_INIT,
    TOPO_STATUS_AUTH,
    TOPO_STATUS_CONFIG,
    TOPO_STATUS_ONLINE,
    TOPO_STATUS_OFFLINE,
    TOPO_STATUS_END,
}TOPO_STATUS_E;

typedef enum
{
    TOPO_DEV_GPS = 0,
    TOPO_DEV_MANAGE,
    TOPO_DEV_LED,
    TOPO_DEV_END, /*TOPO_DEV_Server*/
}TOPO_DEV_E;

typedef struct
{
    TOPO_STATUS_E enStatus;
    UINT32 uiKeepTime;
}TOPO_STATUS_T;

typedef struct
{
    UINT32 uiIpadd;
    UINT32 uiUdpPort;
    UINT8 szUserName[TOPO_MAX_USER_NAME];
    UINT8 szUserPasswd[TOPO_MAX_USER_PASSWD];
}TOPO_BASE_INFO_T;

typedef struct
{
    UINT uiLongitude;
    UINT uiLatitude;
    UINT ucStar;
    UINT uiSpeed;
    UINT uiHigh;
    UINT8 szTime[BUF_SIZE64];
}TOPO_GPS_INFO_T;

typedef struct
{
    TOPO_DEV_E enDev;
    UINT8 szUserId[TOPO_MAX_USER_ID];
    TOPO_GPS_INFO_T stGpsInfo;
}TOPO_GPS_INFO_USER_T;


typedef struct
{
    UINT8 szUserId[TOPO_MAX_USER_ID];
    TOPO_STATUS_T stStatus;
    TOPO_BASE_INFO_T stBaseInfo;
    TOPO_GPS_INFO_T stGpsInfo;
}TOPO_INFO_T;

typedef struct topo_info_list_t
{
    struct topo_info_list_t *pre;
    struct topo_info_list_t *next;
    TOPO_INFO_T stTopo;
}TOPO_INFO_LIST_T;


typedef struct
{
    TOPO_INFO_LIST_T *astTopo[TOPO_MAX_USER];
}TOPO_ENTRY_T;

extern int TOPO_Timer(void *timer, void *data);
extern int PKT_Rev
(
    IN INT fd,
    IN UINT8 *pucBuf,
    IN UINT uiSize,
    IN struct sockaddr_in stSrcAddr
);

#endif /* TOPO_H_ */
