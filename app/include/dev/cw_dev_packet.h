/*-------------------------------------------------------------------------
    topo.h
-------------------------------------------------------------------------*/

#ifndef CW_DEV_PACKET_H_
#define CW_DEV_PACKET_H_

typedef enum
{
    CW_PACKET_OPRATION_REQUEST = 0,
    CW_PACKET_OPRATION_ACK,
    CW_PACKET_OPRATION_TRAP,
    CW_PACKET_OPRATION_TRAP_ACK
}CW_PACKET_OPRATION_T;

typedef enum
{
    CW_PACKET_DEV_INIT = 0,
    CW_PACKET_DEV_AUTH,
    CW_PACKET_DEV_KEEPLIVE,

    CW_PACKET_END
}CW_PACKET_TYPE_T;


typedef struct
{
    UINT8 aucMagic[6];
    UINT8 ucVer;
    UINT8 ucOpration; /* CW_PACKET_OPRATION_T */
    UINT16 usType; /* CW_PACKET_TYPE_T */
    UINT8 aucMac[MAC_ADDR_LEN];
    UINT16 usDevType; /* TOPO_DEV_E */
    UINT8 ucRandomId;
    UINT32 uiSoftwareVersion; /* 应答时作为status用 */
}__attribute__ ((__packed__)) CW_PACKET_HEAD_T;

typedef struct
{
    UINT8 szUserName[TOPO_MAX_USER_NAME];
    UINT8 szUserPasswd[TOPO_MAX_USER_PASSWD];
}__attribute__ ((__packed__)) CW_PACKET_INIT_INFO_T;

typedef struct tagDataBaseGPS
{
    UINT uiLongitude[4];
    UINT uiLatitude[4];
    UINT8 ucStar;
    UINT uiSpeed;
    UINT uiHigh;
}__attribute__ ((__packed__)) CW_PACKET_GPS_INFO_T;

int PACKET_Rev
(
    INT fd,
    UINT8 *pucBuf,
    UINT uiSize,
    struct sockaddr_in stSrcAddr
);

#endif /* CW_DEV_PACKET_H_ */
