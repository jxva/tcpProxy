

#ifndef IPC_PROTOCOL_H_
#define IPC_PROTOCOL_H_

#ifndef __packed
#define __packed __attribute__((__packed__))
#endif
#include "dev/topo.h"

#define	IPC_SERVER_PATH		"/home/soar/Courage/app/bin/ipc_server"
#define	IPC_CLIENT_PATH		"./cgi/"

#define   TRAP_SERVER_PATH   "./.trap_server"
#define   TRAP_FILE_LOCK_PATH   "./.traplock"

typedef enum {
    IPC_SERVICE_MASTER,
    IPC_SERVICE_BUTT
}IPC_SERVICE_TYPE_E;

typedef enum {
	IPC_PING = 0,
    IPC_ARP_INFO,
    IPC_TOPO_INFO,
    IPC_BTREE_TEST,
	IPC_VLAN_DESCRIPTION
}ipc_request_type_t;

typedef enum {
	IPC_CONFIG_GET = 0x01,
	IPC_CONFIG_SET,
	IPC_CONFIG_DEL,
	IPC_CONFIG_NEW
}
ipc_config_access_t;

typedef enum {
	IPC_APPLY_NONE = 0,
	IPC_APPLY_NOW = 0x01,
	IPC_APPLY_COMMIT = 0x02,
	IPC_APPLY_DELAY = 0x04,
}
ipc_apply_option_t;

typedef struct {
    int identify; /* ipc 描述信息 */
    int request_id; /* ipc请求id */
	int ipc_type;
    int ipc_sub_type;
	UINT32 msg_len;
}ipc_request_hdr_t;

typedef struct {
    int identify; /* ipc 描述信息 */
    int request_id; /* ipc请求id */
	int	status;
	int msg_len;
}ipc_acknowledge_hdr_t;

typedef struct __packed{
	int iVid;

}VLAN_DESCRIPTION_S;

typedef struct __packed{
	ipc_request_hdr_t hdr;
	UINT8 access;
    UINT8 apply_option;
	VLAN_DESCRIPTION_S vlan_description;
}
IPC_VLAN_DESCRIPRION_REQ_S;

typedef struct __packed{
	ipc_acknowledge_hdr_t hdr;
	VLAN_DESCRIPTION_S vlan_description;
}
IPC_VLAN_DESCRIPRION_ACK_S;

// Check IPC header and message length
#define	IPC_HEADER_CHECK(type, req) \
	if(req->hdr.msg_len < (sizeof(type) - sizeof(ipc_request_hdr_t))) { \
		hdr.status = IPC_STATUS_ARGV; \
		hdr.msg_len = 0; \
		DBG_ASSERT(0, "Invalid IPC request, message length: %d", req->hdr.msg_len); \
		ipc_send_ack(ipc_fd, &hdr, (ipc_request_hdr_t *)req); \
		return; \
	}
#define sizeof_switch_acl(c) ((int)(((switch_acl_t*)0)->rule) + (c) * sizeof(acl_rule_t))
/*end add by zhouguanhua 2013/10/22*/
#define	REQ_MSGLEN(x) (sizeof(x) - sizeof(ipc_request_hdr_t))
#define	REQ_MSGLEN_R(t, m, mt, c) (offsetof(t, m) + c * sizeof(mt) - sizeof(ipc_request_hdr_t))
#define	ACK_MSGLEN(x) (sizeof(x) - sizeof(ipc_acknowledge_hdr_t))
#define	ACK_MSGLEN_R(t, m, mt, c) (offsetof(t, m) + c * sizeof(mt) - sizeof(ipc_acknowledge_hdr_t))

typedef enum {
    /* olt */
    IPC_ARP_FORCE_AGED,  /*  强制老化一条arp */
    IPC_ARP_UPDATE_AGE_TIME,  /*  更新arp老化时间 */
    IPC_ARP_AGED_TIME,
    IPC_ARP_MAX_NUM,
    IPC_ARP_LEARNING,
    IPC_ARP_SENDING,
    IPC_ARP_IPQAM,
    IPC_ARP_IPQAM_ALL,
    IPC_ARP_BUTT
}IPC_ARP_REQUEST_SUB_TYPE_E;

typedef enum {
    IPC_TOPO_GPS_INFO,

    IPC_TOPO_BUTT
}IPC_TOPO_REQUEST_SUB_TYPE_E;


typedef struct __packed{
    ipc_request_hdr_t hdr;
    UINT8 access;
    UINT8 apply_option;
    UINT32 uiValue;
}IPC_ARP_UINT_REQ_S;

typedef struct __packed{
    ipc_acknowledge_hdr_t hdr;
    UINT32 uiValue;
}IPC_ARP_UINT_ACK_S;

typedef struct __packed{
    ipc_request_hdr_t hdr;
    UINT8 access;
    UINT8 apply_option;
    TOPO_DEV_E enDev;
    UINT8 szUserId[TOPO_MAX_USER_ID];
}IPC_TOPO_REQ_S;

typedef struct __packed{
    ipc_acknowledge_hdr_t hdr;
    TOPO_GPS_INFO_T stBase;
}IPC_TOPO_GPS_INFO_ACK_S;


#endif /* IPC_PROTOCOL_H_ */
