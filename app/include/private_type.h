

#ifndef PRIVATE_TYPE_H_
#define PRIVATE_TYPE_H_
#define IN
#define OUT
#define INOUT

#ifndef NULL
#define NULL 0
#endif

#ifndef TRUE
#define TRUE	1
#endif
#ifndef FALSE
#define FALSE	0
#endif

typedef unsigned short UINT16;

typedef unsigned char UINT8;

typedef char INT8;

typedef void VOID;

typedef unsigned int uint;
typedef unsigned int UINT32;
typedef unsigned int UINT;

#ifndef INT
typedef int INT;
#endif

#ifndef INT32
typedef int INT32;
#endif

#ifndef UINT64
typedef unsigned long long UINT64;
#endif

#define MAC_ADDR_LEN 6
#define	BUF_SIZE32		32
#define	BUF_SIZE64		64

#define	BUF_SIZE256		256

typedef enum {
	IPC_STATUS_OK = 0,
	IPC_STATUS_FAIL,
	IPC_STATUS_ARGV,
	IPC_STATUS_BUSY,
    IPC_STATUS_NOT_FIND,
	IPC_STATUS_APP_INVALID,
	IPC_STATUS_EXIST,
	IPC_STATUS_EXCEED,
	IPC_STATUS_MEM_MALLOC_FAIL,
    IPC_STATUS_CONFLICT,

	IPC_STATUS_END
}
ipc_status_t;


typedef int (*SOCK_AcceptPktDeal)(
     INT fd,
     UINT8 *pucBuf,
     UINT uiSize,
     struct sockaddr_in stSrcAddr
);


#endif /* TIMER_H_ */
