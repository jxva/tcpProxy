/**************server.c******************/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/epoll.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>

#include "private_type.h"
#include "private_log.h"
#include "proxy_type.h"
#include "btree/sys_btree.h"
#include "timer.h"

#include "cjson/cJSON.h"
#include "memory/sys_memory.h"

#define PROXYD_LINK_TUNNEL_SERVER       0
#define PROXYD_LINK_TUNNEL_CLIENT       1
#define PROXYD_LINK_PROXY_SERVER        2
#define PROXYD_LINK_PROXY_CLIENT        3

typedef struct ProxydTopoPacket_t
{
    INT8 aucPktBuf[PROXY_SOCK_MAX_PKT_LEN];
    UINT uiPktLen;
    struct ProxydTopoPacket_t *pstNext;
}PROXYD_TOPO_PACKET_T;

typedef struct
{
    UINT uiDPort;
    UINT uiSPort;
}PROXYD_DATA_PACKET_HEAD_T;


typedef struct
{
    UINT uiSocketFd;
    UINT uiConnectIp;
    UINT16 uiConnectPort;
}SERVER_INFO_T;

typedef struct
{
    UINT uiSocketFd;
    UINT uiIp;
    UINT16 uiPort;
}PROXYD_TOPO_INDEX_T;

typedef struct
{
    PROXYD_TOPO_INDEX_T stServerIndex;
}PROXYD_TOPO_TUNNEL_CLIENT_INFO_T;

typedef struct
{
    PROXYD_TOPO_INDEX_T stTunnelCIndex;

    UINT uiProxydIp;
    UINT16 uiProxydPort;
}PROXYD_TOPO_LINK_SERVER_INFO_T;

typedef struct
{
    PROXYD_TOPO_INDEX_T stServerIndex;  /* client 对应 server的信息 */
    PROXYD_TOPO_LINK_SERVER_INFO_T stServerInfo; /* 管理通道，及远程信息 */
    UINT uiTimer;
    UINT uiProxydFd;
    PROXYD_TOPO_PACKET_T *pstPkt; /* tcp 连接比通常慢，所以需要转存 */
}PROXYD_TOPO_LINK_CLIENT_INFO_T;

typedef struct
{
    PROXYD_TOPO_INDEX_T stIndex;
    UINT8 ucLinkType;
    union
    {
        PROXYD_TOPO_TUNNEL_CLIENT_INFO_T stTunnelClient; /* for PROXYD_LINK_TUNNEL_CLIENT */
        PROXYD_TOPO_LINK_SERVER_INFO_T stLinkServer;  /* for PROXYD_LINK_PROXY_SERVER */
        PROXYD_TOPO_LINK_CLIENT_INFO_T stLinkClint; /* for PROXYD_LINK_PROXY_CLIENT */
    } unDevice;
    PROXY_PACKET_BUFF_T stPktBuf;
}PROXYD_SERVER_TOPO_INFO_T;
typedef struct
{
    int uiEpollFd;
    PROXYD_SERVER_TOPO_INFO_T stTopo;
}PROXYD_TOPO_TIMER_T;


typedef struct ProxyServerTreeNode_t
{
    PROXYD_SERVER_TOPO_INFO_T *pData;
    struct ProxyServerTreeNode_t *Child[BI_TREE_CHILD_NUM];
}BT_PROXYD_SERVER_NODE_ST;

BT_PROXYD_SERVER_NODE_ST g_stProxySocketHead;  /* socket id 作为索引 */
BT_PROXYD_SERVER_NODE_ST g_stProxyIpHead;  /* ip和端口 作为索引 */


extern int PROXYD_LoopCloseDeal(int uiEpollFd, UINT uiFd);
extern void PROXYD_SocketLinkSendToTunnel(INT8 *aucPktBuf, UINT uiLen,PROXYD_SERVER_TOPO_INFO_T *pstTopoInfo);
int PROXYD_ClientDelSync(void *timer, void *data);
void PROXYD_LinkClientTimeoutExit(void *timer);

#if 0
#endif
PROXYD_SERVER_TOPO_INFO_T *PROXYD_FindProxyTopoByFd(UINT uiFd)
{
    BT_INDEX_ST stIndex;
    BT_PROXYD_SERVER_NODE_ST *pstNode = NULL;

    /* 按照socket创建表 */
    memset(&stIndex, 0, sizeof(stIndex));
    stIndex.auiIndex[0] = uiFd;
    stIndex.uiIndexLen = 32;
    pstNode = (BT_PROXYD_SERVER_NODE_ST *)BT_FindBTree((BT_NODE_ST *)&g_stProxySocketHead, &stIndex);
    if(pstNode!= NULL)
    {
        return pstNode->pData;
    }

    return NULL;
}

PROXYD_SERVER_TOPO_INFO_T *PROXYD_FindProxyTopoByIp(UINT uiIp, UINT16 usPort)
{
    BT_INDEX_ST stIndex;
    BT_PROXYD_SERVER_NODE_ST *pstNode = NULL;

    memset(&stIndex, 0, sizeof(stIndex));
    stIndex.auiIndex[0] = uiIp;
    stIndex.auiIndex[1] = usPort;
    stIndex.uiIndexLen = 64;
    pstNode = (BT_PROXYD_SERVER_NODE_ST *)BT_FindBTree((BT_NODE_ST *)&g_stProxyIpHead, &stIndex);
    if(pstNode!= NULL)
    {
        return pstNode->pData;
    }

    return NULL;
}

int PROXYD_AddProxyTopo(PROXYD_SERVER_TOPO_INFO_T *pstData)
{
    BT_INDEX_ST stIndex;
    BT_PROXYD_SERVER_NODE_ST *pstNode = NULL;
    PROXYD_SERVER_TOPO_INFO_T *pstAlloc = NULL;

    /* 按照socket创建表 */
    memset(&stIndex, 0, sizeof(stIndex));
    stIndex.auiIndex[0] = pstData->stIndex.uiSocketFd;
    stIndex.uiIndexLen = 32;
    pstNode = (BT_PROXYD_SERVER_NODE_ST *)BT_AddBTree((BT_NODE_ST *)&g_stProxySocketHead, &stIndex);
    if(pstNode!= NULL)
    {
        pstNode->pData = (PROXYD_SERVER_TOPO_INFO_T *)MEM_MALLOC(sizeof(PROXYD_SERVER_TOPO_INFO_T));
        if(NULL == pstNode->pData)
        {
            CW_SendErr( "malloc error! size=%d", sizeof(PROXYD_SERVER_TOPO_INFO_T));
            return IPC_STATUS_MEM_MALLOC_FAIL;
        }
        memcpy(pstNode->pData, pstData, sizeof(PROXYD_SERVER_TOPO_INFO_T));
        pstAlloc = pstNode->pData;
    }

    /* 按照ip创建表 */
    memset(&stIndex, 0, sizeof(stIndex));
    stIndex.auiIndex[0] = pstData->stIndex.uiIp;
    stIndex.auiIndex[1] = pstData->stIndex.uiPort;
    stIndex.uiIndexLen = 64;
    pstNode = (BT_PROXYD_SERVER_NODE_ST *)BT_AddBTree((BT_NODE_ST *)&g_stProxyIpHead, &stIndex);
    if(pstNode!= NULL)
    {
        if(NULL == pstAlloc)
        {
            pstNode->pData = (PROXYD_SERVER_TOPO_INFO_T *)MEM_MALLOC(sizeof(PROXYD_SERVER_TOPO_INFO_T));
            if(NULL == pstNode->pData)
            {
                CW_SendErr( "malloc error! size=%d", sizeof(PROXYD_SERVER_TOPO_INFO_T));
                return IPC_STATUS_MEM_MALLOC_FAIL;
            }
            memcpy(pstNode->pData, pstData, sizeof(PROXYD_SERVER_TOPO_INFO_T));
            pstAlloc = pstNode->pData;
        }
        else
        {
            pstNode->pData = pstAlloc;
        }
    }

    CW_SendInfo( "ip=%s, port=%d fd=%d",
                  inet_ntoa(*(struct in_addr*)&(pstData->stIndex.uiIp)),
                  pstData->stIndex.uiPort, pstData->stIndex.uiSocketFd);
    return IPC_STATUS_OK;
}
static int ProxyTopoDelFuncCommon(void *pInData, BT_NODE_ST *pstNode)
{
    if((NULL != pstNode) && (NULL != pstNode->pData))
    {
        /* 由于两处地方都用了这一块内存，所以统一释放 */
        pstNode->pData = NULL;
    }
    return TRUE;
}

int PROXYD_DelProxyTopo(PROXYD_SERVER_TOPO_INFO_T *pstData)
{
    BT_INDEX_ST stIndex;
    BT_PROXYD_SERVER_NODE_ST *pstNode = NULL;
    PROXYD_SERVER_TOPO_INFO_T *pstAlloc = NULL;
    PROXYD_TOPO_TIMER_T *pstTimer=NULL;

    pstAlloc = PROXYD_FindProxyTopoByFd(pstData->stIndex.uiSocketFd);
    if(pstAlloc == NULL)
    {
        return IPC_STATUS_OK;
    }
    CW_SendInfo( "ip=%s, port=%d fd=%d",
                  inet_ntoa(*(struct in_addr*)&(pstData->stIndex.uiIp)),
                  pstData->stIndex.uiPort, pstData->stIndex.uiSocketFd);
    /* 按照socket创建表 */
    memset(&stIndex, 0, sizeof(stIndex));
    stIndex.auiIndex[0] = pstData->stIndex.uiSocketFd;
    stIndex.uiIndexLen = 32;
    BT_DelTree((BT_NODE_ST *)&g_stProxySocketHead, &stIndex, NULL,ProxyTopoDelFuncCommon);

    /* 按照ip创建表 */
    memset(&stIndex, 0, sizeof(stIndex));
    stIndex.auiIndex[0] = pstData->stIndex.uiIp;
    stIndex.auiIndex[1] = pstData->stIndex.uiPort;
    stIndex.uiIndexLen = 64;
    BT_DelTree((BT_NODE_ST *)&g_stProxyIpHead, &stIndex, NULL,ProxyTopoDelFuncCommon);

    switch(pstAlloc->ucLinkType)
    {
        case PROXYD_LINK_TUNNEL_CLIENT:
        case PROXYD_LINK_PROXY_CLIENT:
            pstTimer = (PROXYD_TOPO_TIMER_T *)MEM_MALLOC(sizeof(PROXYD_TOPO_TIMER_T));
            if(NULL == pstTimer)
            {
                CW_SendErr( "malloc error! size=%d", sizeof(PROXYD_TOPO_TIMER_T));
                return IPC_STATUS_MEM_MALLOC_FAIL;
            }
            pstTimer->uiEpollFd = 0;
            memcpy(&pstTimer->stTopo, pstAlloc, sizeof(pstTimer->stTopo));
            timer_register(PROXY_SYNC_TIMEOUT*1000, 0, (timer_func_t)PROXYD_ClientDelSync , pstTimer, PROXYD_LinkClientTimeoutExit, "tunnel_client_sync_timeout");
            break;
         default:
            break;
    }
    if(PROXYD_LINK_PROXY_CLIENT == pstAlloc->ucLinkType)
    {
        PROXYD_SocketLinkPktBufDel(pstAlloc->unDevice.stLinkClint.pstPkt);
        pstAlloc->unDevice.stLinkClint.pstPkt = NULL;
    }
    if(NULL != pstAlloc->stPktBuf.s_LastPktBuf)
    {
        MEM_FREE(pstAlloc->stPktBuf.s_LastPktBuf);
    }

    MEM_FREE(pstAlloc);
    return IPC_STATUS_OK;
}

static int ProxyTopoFreeFuncCommon(void *pInData, BT_NODE_ST *pstNode)
{
    PROXYD_SERVER_TOPO_INFO_T *pstAlloc = NULL;
    if((NULL != pstNode) && (NULL != pstNode->pData))
    {
        pstAlloc = pstNode->pData;
        pstNode->pData = NULL;

        if(PROXYD_LINK_PROXY_CLIENT == pstAlloc->ucLinkType)
        {
            PROXYD_SocketLinkPktBufDel(pstAlloc->unDevice.stLinkClint.pstPkt);
            pstAlloc->unDevice.stLinkClint.pstPkt = NULL;
        }
        if(NULL != pstAlloc->stPktBuf.s_LastPktBuf)
        {
            MEM_FREE(pstAlloc->stPktBuf.s_LastPktBuf);
        }
        close(pstAlloc->stIndex.uiSocketFd);
        CW_SendInfo( "ip=%s, port=%d fd=%d",
                      inet_ntoa(*(struct in_addr*)&(pstAlloc->stIndex.uiIp)),
                      pstAlloc->stIndex.uiPort, pstAlloc->stIndex.uiSocketFd);

        MEM_FREE(pstAlloc);
    }
    return TRUE;
}

int PROXYD_FreeProxyTopo()
{

    BT_DelBTreeByCheck((BT_NODE_ST *)&g_stProxySocketHead, NULL,
                        BT_GetAllBTreeFuncCheckNone, ProxyTopoFreeFuncCommon);

    BT_DelBTreeByCheck((BT_NODE_ST *)&g_stProxyIpHead, NULL,
                        BT_GetAllBTreeFuncCheckNone, ProxyTopoDelFuncCommon);

    return IPC_STATUS_OK;
}

int PROXYD_FreeProxyTopoByTunnelClientCheck(void *TreeData, void *CheckData)
{
    PROXYD_SERVER_TOPO_INFO_T *pstCheck = CheckData;
    PROXYD_SERVER_TOPO_INFO_T *pstData = CheckData;

    if((NULL == pstCheck) || (NULL == pstData))
    {
        CW_SendErr( "Point is NULL!");
        return FALSE;
    }

    switch(pstData->ucLinkType)
    {
        case PROXYD_LINK_PROXY_SERVER:
            if(0 != memcpy(&pstData->unDevice.stLinkServer.stTunnelCIndex,
                            &pstCheck->stIndex, sizeof(pstCheck->stIndex)))
            {
                return FALSE;
            }
            break;
        case PROXYD_LINK_PROXY_CLIENT:
            if(0 != memcpy(&pstData->unDevice.stLinkClint.stServerInfo.stTunnelCIndex,
                            &pstCheck->stIndex, sizeof(pstCheck->stIndex)))
            {
                return FALSE;
            }
            break;
         default:
            break;

    }

    return TRUE;
}

int PROXYD_FreeProxyTopoByTunnelClient(PROXYD_SERVER_TOPO_INFO_T *pstTopo)
{

    BT_DelBTreeByCheck((BT_NODE_ST *)&g_stProxySocketHead, pstTopo,
                        PROXYD_FreeProxyTopoByTunnelClientCheck, ProxyTopoFreeFuncCommon);

    BT_DelBTreeByCheck((BT_NODE_ST *)&g_stProxyIpHead, pstTopo,
                        PROXYD_FreeProxyTopoByTunnelClientCheck, ProxyTopoDelFuncCommon);

    return IPC_STATUS_OK;
}

int PROXYD_FreeProxyTopoByProxyClient(PROXYD_SERVER_TOPO_INFO_T *pstTopo)
{
    INT8 aucPktBuf[PROXY_SOCK_MAX_PKT_LEN] = {0};
    PROXY_PACKET_HEAD_T *pstHead = aucPktBuf;
    INT8 *pstManage = aucPktBuf + sizeof(PROXY_PACKET_HEAD_T);
    cJSON *pstJson = NULL;
    memcpy(pstHead->aucMagic, g_szMagic, sizeof(g_szMagic));
    pstHead->ucOpration = PROXY_PACKET_OP_MANAGE;

    pstJson = cJSON_CreateObject();
    cJSON_AddNumberToObject(pstJson, "ManageType",PROXY_PKT_TYPE_RELEASE);

    cJSON_AddNumberToObject(pstJson, "Status",0);
    cJSON_AddNumberToObject(pstJson, "LinkClientFd",pstTopo->stIndex.uiSocketFd);
    cJSON_AddNumberToObject(pstJson, "LinkClientIp",pstTopo->stIndex.uiIp);
    cJSON_AddNumberToObject(pstJson, "LinkClientPort",pstTopo->stIndex.uiPort);
    cJSON_AddNumberToObject(pstJson, "LinkServerPort",
                            pstTopo->unDevice.stLinkClint.stServerIndex.uiPort);
    cJSON_AddNumberToObject(pstJson, "ProxydFd",pstTopo->unDevice.stLinkClint.uiProxydFd);

    char *szOutJson = cJSON_Print(pstJson);

    strncpy(pstManage, szOutJson, PROXY_SOCK_MAX_PKT_LEN-sizeof(PROXY_PACKET_HEAD_T));

    pstHead->uiPktLen = htonl(sizeof(PROXY_PACKET_HEAD_T)+strlen(szOutJson));

    write(pstTopo->unDevice.stLinkClint.stServerInfo.stTunnelCIndex.uiSocketFd,
         aucPktBuf, sizeof(PROXY_PACKET_HEAD_T)+strlen(szOutJson));
    free(szOutJson);
    cJSON_Delete(pstJson);

    return IPC_STATUS_OK;
}

int PROXYD_ClientDelSync(void *timer, void *data)
{
    PROXYD_TOPO_TIMER_T *pstTimer = data;
    switch(pstTimer->stTopo.ucLinkType)
    {
        case PROXYD_LINK_TUNNEL_CLIENT:
            PROXYD_FreeProxyTopoByTunnelClient(&pstTimer->stTopo);
            break;
        case PROXYD_LINK_PROXY_CLIENT:
            PROXYD_FreeProxyTopoByProxyClient(&pstTimer->stTopo);
            break;
         default:
            break;

    }
    return TIMER_RUN_ONCE;
}

#if 0
#endif

void PROXYD_SocketTunnelAccept(int uiEpollFd, PROXYD_SERVER_TOPO_INFO_T *pstTopoInfo)
{
    struct sockaddr_in stSrcAddr;
    socklen_t uiAddrLen = sizeof(stSrcAddr);
    int uiListenFd = 0;
    UINT32 uiFlags = EPOLLIN | EPOLLRDHUP | EPOLLHUP;
    PROXYD_SERVER_TOPO_INFO_T stTmp;

    uiListenFd = accept(pstTopoInfo->stIndex.uiSocketFd, (struct sockaddr*) &stSrcAddr, &uiAddrLen);
    fcntl(uiListenFd, F_SETFL, O_NONBLOCK);

    CW_SendDebug("Accept from server %d:ip=%s:%d, port=%d fd=%d\n",
            pstTopoInfo->stIndex.uiSocketFd,
            inet_ntoa(*(struct in_addr*)&(stSrcAddr.sin_addr)),
            stSrcAddr.sin_addr.s_addr,
            ntohs(stSrcAddr.sin_port), uiListenFd);

    memset(&stTmp, 0, sizeof(stTmp));
    stTmp.stIndex.uiSocketFd = uiListenFd;
    stTmp.stIndex.uiPort = ntohs(stSrcAddr.sin_port);
    stTmp.stIndex.uiIp = stSrcAddr.sin_addr.s_addr;
    stTmp.ucLinkType = PROXYD_LINK_TUNNEL_CLIENT;

    memcpy(&stTmp.unDevice.stTunnelClient.stServerIndex,
           &pstTopoInfo->stIndex, sizeof(pstTopoInfo->stIndex));

    PROXYD_AddProxyTopo(&stTmp);

    SOCKET_AddEpoll(uiEpollFd, uiListenFd, uiFlags);
    return;
}
#if 0
#endif
int  PROXYD_SocketCreateLinkServer
(
    IN int uiEpollFd,
    IN PROXYD_SERVER_TOPO_INFO_T *pstTopoInfo,
    IN PROXY_PACKET_MIRROR_T *pstPktManage,
    OUT PROXYD_SERVER_TOPO_INFO_T *pstNewServer
)
{
    int uiProxyFd = -1;
    UINT16 usProxyPort = 0;
    UINT uiProxyIp = INADDR_ANY;
    UINT32 uiFlags = EPOLLIN | EPOLLRDHUP | EPOLLHUP;
    PROXYD_SERVER_TOPO_INFO_T *pstTopo = NULL;

    usProxyPort = (pstPktManage->uiLinkServerPort);
    pstTopo = PROXYD_FindProxyTopoByIp(uiProxyIp, usProxyPort);
    if(NULL != pstTopo)
    {
        if((pstTopo->ucLinkType == PROXYD_LINK_PROXY_SERVER) &&
            (0==memcmp(&pstTopo->unDevice.stLinkServer.stTunnelCIndex,
                        &pstTopoInfo->stIndex, sizeof(pstTopoInfo->stIndex))))
        {
            return IPC_STATUS_EXIST;
        }
        else
        {
            CW_SendErr( "port conflict, ip=%s, port=%d", inet_ntoa(*(struct in_addr*)&(uiProxyIp)), usProxyPort);
            return IPC_STATUS_CONFLICT;
        }
    }


    uiProxyFd = SOCKET_TcpServerCreate(uiProxyIp ,usProxyPort);
    if(uiProxyFd==-1)
    {
        CW_SendErr( "SOCKET_TcpServerCreate error, ip=%s, port=%d", inet_ntoa(*(struct in_addr*)&(uiProxyIp)), usProxyPort);
        return IPC_STATUS_FAIL;
    }

    memset(pstNewServer, 0, sizeof(PROXYD_SERVER_TOPO_INFO_T));
    pstNewServer->stIndex.uiSocketFd= uiProxyFd;
    pstNewServer->stIndex.uiPort = usProxyPort;
    pstNewServer->stIndex.uiIp = uiProxyIp;
    pstNewServer->ucLinkType = PROXYD_LINK_PROXY_SERVER;

    memcpy(&pstNewServer->unDevice.stLinkServer.stTunnelCIndex,
           &pstTopoInfo->stIndex, sizeof(pstTopoInfo->stIndex));
    pstNewServer->unDevice.stLinkServer.uiProxydPort = pstPktManage->uiProxyfPort;
    pstNewServer->unDevice.stLinkServer.uiProxydIp = pstPktManage->uiProxyfIp;

    PROXYD_AddProxyTopo(pstNewServer);

    SOCKET_AddEpoll(uiEpollFd, uiProxyFd, uiFlags);
    return IPC_STATUS_OK;
}


int  PROXYD_SocketTunnelPktDeal
(
    int uiEpollFd,
    PROXYD_SERVER_TOPO_INFO_T *pstTopoInfo,
    UINT8 *aucPktBuf,
    UINT uiPktLen
)
{
    UINT8 *pstManage = NULL;
    PROXYD_SERVER_TOPO_INFO_T stNewServer;
    cJSON *pstJson = NULL;
    cJSON *pstSub = NULL;
    UINT uiRet = 0;
    PROXY_PACKET_MIRROR_T stMirrorInfo;
    char *szOutJson = NULL;
    PROXY_PACKET_LINK_REQUEST_T stLinkReqInfo;
    PROXY_PACKET_HEAD_T *pstHead = NULL;

    if (uiPktLen < sizeof(PROXY_PACKET_HEAD_T))
    {
        CW_SendErr("ManagePkt is too short!! %d\n",
                      uiPktLen);
        return IPC_STATUS_FAIL;
    }
    pstHead = (PROXY_PACKET_HEAD_T *)aucPktBuf;
    pstManage = aucPktBuf+sizeof(PROXY_PACKET_HEAD_T);

    CW_SendDebug("rev = %s\n", pstManage);

    pstJson=cJSON_Parse(pstManage);
    if (!pstJson)
    {
        CW_SendErr("Packet Error: [%s], str=%s\n",cJSON_GetErrorPtr(), pstManage);
        return IPC_STATUS_ARGV;
    }

    pstSub = cJSON_GetObjectItem(pstJson, "ManageType");
    if (NULL == pstSub)
    {
        CW_SendErr("Can not find Opration!!\n");
        return IPC_STATUS_ARGV;
    }

    switch(pstSub->valueint)
    {
        case PROXY_PKT_TYPE_TUNNEL_CFG:
            memset(&stMirrorInfo, 0, sizeof(stMirrorInfo));
            PROXY_SocketGetLinkServerInfo(pstJson, &stMirrorInfo);
            uiRet = PROXYD_SocketCreateLinkServer(uiEpollFd, pstTopoInfo,&stMirrorInfo, &stNewServer);

            cJSON_SetIntValue(pstSub, PROXY_PKT_TYPE_TUNNEL_CFG_ACK);

            pstSub = cJSON_GetObjectItem(pstJson, "Status");
            if (NULL == pstSub)
            {
                CW_SendErr("Can not find Status!!\n");
                return IPC_STATUS_ARGV;
            }
            pstSub->valueint = uiRet;

            cJSON_SetIntValue(pstSub, uiRet);

            szOutJson = cJSON_Print(pstJson);
            strncpy(pstManage, szOutJson, PROXY_SOCK_MAX_PKT_LEN-sizeof(PROXY_PACKET_HEAD_T));

            pstHead->uiPktLen = htonl(sizeof(PROXY_PACKET_HEAD_T)+strlen(szOutJson));

            write(pstTopoInfo->stIndex.uiSocketFd, aucPktBuf, sizeof(PROXY_PACKET_HEAD_T)+strlen(szOutJson));
            free(szOutJson);
            break;
        case PROXY_PKT_TYPE_CONNECT_ACK:
            memset(&stLinkReqInfo, 0, sizeof(stLinkReqInfo));
            PROXY_SocketGetLinkAckInfo(pstJson, &stLinkReqInfo);
            PROXYD_SocketLinkAckPktDeal(&stLinkReqInfo);

            break;

        case PROXY_PKT_TYPE_RELEASE:
            memset(&stLinkReqInfo, 0, sizeof(stLinkReqInfo));
            PROXY_SocektGetReleaseInfo(pstJson, &stLinkReqInfo);
            PROXYD_SERVER_TOPO_INFO_T *pstAlloc = NULL;
            pstAlloc = PROXYD_FindProxyTopoByFd(stLinkReqInfo.uiLinkClientFd);
            if(pstAlloc != NULL)
            {
                PROXYD_DelProxyTopo(pstAlloc);
                close(stLinkReqInfo.uiLinkClientFd);
            }
            break;

        default:
            break;
    }

    cJSON_Delete(pstJson);

    return IPC_STATUS_OK;

}
int  PROXYD_SocketTunnelDataPktDeal
(
    PROXYD_SERVER_TOPO_INFO_T *pstTopoInfo,
    UINT8 *aucPktBuf,
    UINT uiPktLen
)
{
    PROXY_PACKET_HEAD_T *pstHead = NULL;
    PROXY_PACKET_DATA_CTL_T *pstDataCtl = NULL;
    UINT uiHeadLen = sizeof(PROXY_PACKET_HEAD_T) + sizeof(PROXY_PACKET_DATA_CTL_T);
    INT8 *aucPktData = aucPktBuf + uiHeadLen;
    UINT uiDFd = -1;
    PROXYD_SERVER_TOPO_INFO_T *pstTopo = NULL;

    CW_SendDebug("Read data from %d:ip=%s:%d port=%d\n",
              pstTopoInfo->stIndex.uiSocketFd,
              inet_ntoa(*(struct in_addr*)&(pstTopoInfo->stIndex.uiIp)),
              pstTopoInfo->stIndex.uiIp,
              (pstTopoInfo->stIndex.uiPort));
    PKT_Dump(aucPktBuf, uiPktLen);
    CW_SendDump("\n");

    pstHead = (PROXY_PACKET_HEAD_T *)aucPktBuf;
    pstDataCtl = aucPktBuf+sizeof(PROXY_PACKET_HEAD_T);
    uiDFd = ntohl(pstDataCtl->uiLinkFd);

    pstTopo = PROXYD_FindProxyTopoByFd(uiDFd);
    if(NULL == pstTopo)
    {
        CW_SendErr("Can not find topo, fd=%d!!\n", uiDFd);
        return IPC_STATUS_NOT_FIND;
    }
    else
    {

        CW_SendDebug("send data to %d(%d):ip=%s:%d, port=%d\n",
                  pstTopo->stIndex.uiSocketFd, uiDFd,
                  inet_ntoa(*(struct in_addr*)&(pstTopo->stIndex.uiIp)),
                  pstTopo->stIndex.uiIp,
                  (pstTopo->stIndex.uiPort));
    }

    PKT_Dump(aucPktData, uiPktLen-uiHeadLen);
    CW_SendDump("\n");

    write(pstTopo->stIndex.uiSocketFd, aucPktData, uiPktLen-uiHeadLen);

    return IPC_STATUS_OK;
}
void PROXYD_SocketTunnelRead(int uiEpollFd, PROXYD_SERVER_TOPO_INFO_T *pstTopoInfo, INT8 *aucPktBuf, int uiPktLen)
{
    //INT8 aucPktBuf[PROXY_SOCK_MAX_PKT_LEN];
    PROXY_PACKET_HEAD_T *pstHead = NULL;
    //int uiPktLen = read(pstTopoInfo->stIndex.uiSocketFd, aucPktBuf, PROXY_SOCK_MAX_PKT_LEN);
    if (uiPktLen < sizeof(PROXY_PACKET_HEAD_T))
    {
        return;
    }
    CW_SendDebug("Read from server %d:ip=%s:%d port=%d, len=%d\n",
                  pstTopoInfo->stIndex.uiSocketFd,
                  inet_ntoa(*(struct in_addr*)&(pstTopoInfo->stIndex.uiIp)),
                  pstTopoInfo->stIndex.uiIp,
                  (pstTopoInfo->stIndex.uiPort), uiPktLen);

    pstHead = aucPktBuf;
    if(0 != memcmp(pstHead->aucMagic, g_szMagic, sizeof(g_szMagic)))
    {
        return;
    }

    switch(pstHead->ucOpration)
    {
        case PROXY_PACKET_OP_MANAGE:
            PROXYD_SocketTunnelPktDeal(uiEpollFd, pstTopoInfo,
                                      aucPktBuf,
                                      uiPktLen);
            break;
        case PROXY_PACKET_OP_DATA:
            PROXYD_SocketTunnelDataPktDeal(pstTopoInfo,aucPktBuf,uiPktLen);
            break;
        default:
            break;
    }

    return;
}
#if 0
#endif
int  PROXYD_SocketLinkPktBufDel
(
    IN PROXYD_TOPO_PACKET_T *pstInfo
)
{
    if(NULL != pstInfo)
    {
        PROXYD_SocketLinkPktBufDel(pstInfo->pstNext);
        MEM_FREE(pstInfo);
        pstInfo = NULL;
    }
    return IPC_STATUS_OK;
}

int  PROXYD_SocketLinkPktBufSend
(
    INOUT PROXYD_TOPO_PACKET_T *pstInfo,
    IN PROXYD_SERVER_TOPO_INFO_T * pstTopo
)
{
    if(NULL != pstInfo)
    {
        PROXYD_SocketLinkSendToTunnel(pstInfo->aucPktBuf, pstInfo->uiPktLen, pstTopo);
        PROXYD_SocketLinkPktBufSend(pstInfo->pstNext, pstTopo);
        MEM_FREE(pstInfo);
        pstInfo = NULL;
    }
    return IPC_STATUS_OK;
}

int  PROXYD_SocketLinkAckPktDeal
(
    IN int uiEpollFd,
    IN PROXY_PACKET_LINK_REQUEST_T *pstInfo
)
{
    PROXYD_SERVER_TOPO_INFO_T * pstTopo = NULL;
    PROXYD_TOPO_PACKET_T *pstPkt = NULL;
    pstTopo = PROXYD_FindProxyTopoByFd(pstInfo->uiLinkClientFd);
    if(NULL == pstTopo)
    {
        CW_SendErr("Can not find topo, fd=%d!!\n", pstInfo->uiLinkClientFd);
        return IPC_STATUS_ARGV;
    }
    if(pstTopo->unDevice.stLinkClint.uiTimer >= 0)
    {
        timer_cancel(pstTopo->unDevice.stLinkClint.uiTimer);
        pstTopo->unDevice.stLinkClint.uiTimer = -1;
    }

    if(IPC_STATUS_OK != pstInfo->usStatus)
    {
        CW_SendErr("Connect to proxyd server error, fd=%d!!\n", pstInfo->uiLinkClientFd);

        SOCKET_DelEpoll(uiEpollFd, pstTopo->stIndex.uiSocketFd, PROXYD_LoopCloseDeal);
        return IPC_STATUS_ARGV;
    }

    pstTopo->unDevice.stLinkClint.uiProxydFd = pstInfo->uiProxydFd;

    pstPkt = pstTopo->unDevice.stLinkClint.pstPkt;
    PROXYD_SocketLinkPktBufSend(pstPkt, pstTopo);
    pstTopo->unDevice.stLinkClint.pstPkt = NULL;

    return IPC_STATUS_OK;
}

void PROXYD_LinkClientTimeoutExit(void *timer)
{
    timer_element_t *tmp = timer;
    MEM_FREE(tmp->data);
    return;
}

int PROXYD_LinkClientTimeout(void *timer, void *data)
{
    PROXYD_TOPO_TIMER_T *pstTimer = data;

    SOCKET_DelEpoll(pstTimer->uiEpollFd, pstTimer->stTopo.stIndex.uiSocketFd, PROXYD_LoopCloseDeal);
    return TIMER_RUN_ONCE;
}
void PROXYD_SocketSendConnectRequest(PROXYD_SERVER_TOPO_INFO_T *pstTopoInfo)
{
    INT8 aucPktBuf[PROXY_SOCK_MAX_PKT_LEN] = {0};
    PROXY_PACKET_HEAD_T *pstHead = aucPktBuf;
    INT8 *pstManage = aucPktBuf + sizeof(PROXY_PACKET_HEAD_T);
    cJSON *pstJson = NULL;
    memcpy(pstHead->aucMagic, g_szMagic, sizeof(g_szMagic));
    pstHead->ucOpration = PROXY_PACKET_OP_MANAGE;

    pstJson = cJSON_CreateObject();
    cJSON_AddNumberToObject(pstJson, "ManageType",PROXY_PKT_TYPE_CONNECT);

    cJSON_AddNumberToObject(pstJson, "Status",0);
    cJSON_AddNumberToObject(pstJson, "LinkClientFd",pstTopoInfo->stIndex.uiSocketFd);
    cJSON_AddNumberToObject(pstJson, "LinkClientIp",pstTopoInfo->stIndex.uiIp);
    cJSON_AddNumberToObject(pstJson, "LinkClientPort",pstTopoInfo->stIndex.uiPort);
    cJSON_AddNumberToObject(pstJson, "LinkServerPort",
                            pstTopoInfo->unDevice.stLinkClint.stServerIndex.uiPort);
    cJSON_AddNumberToObject(pstJson, "ProxydFd",-1);

    char *szOutJson = cJSON_Print(pstJson);

    strncpy(pstManage, szOutJson, PROXY_SOCK_MAX_PKT_LEN-sizeof(PROXY_PACKET_HEAD_T));

    pstHead->uiPktLen = htonl(sizeof(PROXY_PACKET_HEAD_T)+strlen(szOutJson));

    write(pstTopoInfo->unDevice.stLinkClint.stServerInfo.stTunnelCIndex.uiSocketFd,
         aucPktBuf, sizeof(PROXY_PACKET_HEAD_T)+strlen(szOutJson));
    free(szOutJson);
    cJSON_Delete(pstJson);
    return ;
}

void PROXYD_SocketPorxyAccept(int uiEpollFd, PROXYD_SERVER_TOPO_INFO_T *pstTopoInfo)
{
    struct sockaddr_in stSrcAddr;
    socklen_t uiAddrLen = sizeof(stSrcAddr);
    int uiListenFd = 0;
    UINT32 uiFlags = EPOLLIN | EPOLLRDHUP | EPOLLHUP;
    PROXYD_SERVER_TOPO_INFO_T stTmp;
    PROXYD_SERVER_TOPO_INFO_T *pstTopo = NULL;
    PROXYD_TOPO_TIMER_T *pstTimer = NULL;

    uiListenFd = accept(pstTopoInfo->stIndex.uiSocketFd, (struct sockaddr*) &stSrcAddr, &uiAddrLen);
    fcntl(uiListenFd, F_SETFL, O_NONBLOCK);

    CW_SendDebug("Accept from server %d:ip=%s:%d, port=%d,fd=%d\n",
            pstTopoInfo->stIndex.uiSocketFd,
            inet_ntoa(*(struct in_addr*)&(stSrcAddr.sin_addr)),
            stSrcAddr.sin_addr.s_addr,
            ntohs(stSrcAddr.sin_port), uiListenFd);

    memset(&stTmp, 0, sizeof(stTmp));
    stTmp.stIndex.uiSocketFd= uiListenFd;
    stTmp.stIndex.uiPort= ntohs(stSrcAddr.sin_port);
    stTmp.stIndex.uiIp = stSrcAddr.sin_addr.s_addr;
    stTmp.ucLinkType = PROXYD_LINK_PROXY_CLIENT;

    memcpy(&stTmp.unDevice.stLinkClint.stServerIndex,
           &pstTopoInfo->stIndex, sizeof(pstTopoInfo->stIndex));

    memcpy(&stTmp.unDevice.stLinkClint.stServerInfo,
           &pstTopoInfo->unDevice.stLinkServer, sizeof(pstTopoInfo->unDevice.stLinkServer));
    stTmp.unDevice.stLinkClint.uiProxydFd = -1;
    PROXYD_AddProxyTopo(&stTmp);
    pstTopo = PROXYD_FindProxyTopoByFd(uiListenFd);
    if(NULL == pstTopo)
    {
        CW_SendErr("Can not find topo, fd=%d!!\n", uiListenFd);
        return ;
    }
    SOCKET_AddEpoll(uiEpollFd, uiListenFd, uiFlags);

    PROXYD_SocketSendConnectRequest(pstTopo);

    pstTimer = (PROXYD_TOPO_TIMER_T *)MEM_MALLOC(sizeof(PROXYD_TOPO_TIMER_T));
    if(NULL == pstTimer)
    {
        CW_SendErr( "malloc error! size=%d", sizeof(PROXYD_TOPO_TIMER_T));
        return ;
    }
    pstTimer->uiEpollFd = uiEpollFd;
    memcpy(&pstTimer->stTopo, pstTopo, sizeof(pstTimer->stTopo));

    pstTopo->unDevice.stLinkClint.uiTimer = timer_register(PROXY_LINK_CLIENT_TIMEOUT*1000, 0, (timer_func_t)PROXYD_LinkClientTimeout , pstTimer, PROXYD_LinkClientTimeoutExit, "proxy_client_timeout");

    return;
}
#if 0
#endif
void PROXYD_SocketLinkSendToTunnel(INT8 * aucPktBuf, UINT uiLen,PROXYD_SERVER_TOPO_INFO_T *pstTopoInfo)
{
    PROXY_PACKET_HEAD_T *pstHead = NULL;
    PROXY_PACKET_DATA_CTL_T *pstDataCtl = NULL;
    pstHead = (PROXY_PACKET_HEAD_T *)aucPktBuf;
    pstDataCtl = (PROXY_PACKET_DATA_CTL_T *)(aucPktBuf+sizeof(PROXY_PACKET_HEAD_T));
    memcpy(pstHead->aucMagic, g_szMagic, sizeof(g_szMagic));
    pstHead->ucOpration = PROXY_PACKET_OP_DATA;
    pstDataCtl->uiLinkFd = htonl(pstTopoInfo->stIndex.uiSocketFd);
    pstDataCtl->uiProxyFd = htonl(pstTopoInfo->unDevice.stLinkClint.uiProxydFd);

    pstHead->uiPktLen = htonl(uiLen);

    CW_SendDebug("Send to fd = %d, %d\n",pstDataCtl->uiLinkFd, pstDataCtl->uiProxyFd);
    PKT_Dump(aucPktBuf, uiLen);

    write(pstTopoInfo->unDevice.stLinkClint.stServerInfo.stTunnelCIndex.uiSocketFd,
         aucPktBuf, uiLen);
    return;
}

void PROXYD_SocketLinkRead(int uiEpollFd, PROXYD_SERVER_TOPO_INFO_T *pstTopoInfo)
{
    INT8 aucPktBuf[PROXY_SOCK_MAX_PKT_LEN];
    UINT uiHeadLen = sizeof(PROXY_PACKET_HEAD_T) + sizeof(PROXY_PACKET_DATA_CTL_T);
    INT8 *aucPktData = aucPktBuf + uiHeadLen;
    PROXYD_TOPO_PACKET_T **pstPkt = NULL;
    PROXYD_TOPO_PACKET_T *pstPktLast = NULL;

    int uiPktLen = read(pstTopoInfo->stIndex.uiSocketFd, aucPktData, PROXY_SOCK_MAX_PKT_LEN-uiHeadLen);
    if(0 == uiPktLen)
    {
        SOCKET_DelEpoll(uiEpollFd, pstTopoInfo->stIndex.uiSocketFd, PROXYD_LoopCloseDeal);
        return;
    }
    CW_SendDebug("Read from server %d:ip=%s:%d, port=%d\n",
                  pstTopoInfo->stIndex.uiSocketFd,
                  inet_ntoa(*(struct in_addr*)&(pstTopoInfo->stIndex.uiIp)),
                  pstTopoInfo->stIndex.uiIp,
                  (pstTopoInfo->stIndex.uiPort));

    PKT_Dump(aucPktData, uiPktLen);

    if(-1 == pstTopoInfo->unDevice.stLinkClint.uiProxydFd)
    {
        CW_SendDebug("Link Client is not ready!!! %d:ip=%s:%d, port=%d\n",
                  pstTopoInfo->stIndex.uiSocketFd,
                  inet_ntoa(*(struct in_addr*)&(pstTopoInfo->stIndex.uiIp)),
                  pstTopoInfo->stIndex.uiIp,
                  (pstTopoInfo->stIndex.uiPort));

        pstPkt = &(pstTopoInfo->unDevice.stLinkClint.pstPkt);
        pstPktLast = *pstPkt;
        while(NULL != *pstPkt)
        {
            pstPktLast = *pstPkt;
            pstPkt = &((*pstPkt)->pstNext);
        }
        *pstPkt = (PROXYD_TOPO_PACKET_T *)MEM_MALLOC(sizeof(PROXYD_TOPO_PACKET_T));
        if(NULL == (*pstPkt))
        {
            CW_SendErr("malloc error, size=%d!!\n", sizeof(PROXYD_TOPO_PACKET_T));
            return;
        }

        memset(*pstPkt, 0, sizeof(PROXYD_TOPO_PACKET_T));
        (*pstPkt)->uiPktLen = uiPktLen+uiHeadLen;
        memcpy((*pstPkt)->aucPktBuf, aucPktBuf, sizeof(aucPktBuf));
        if(NULL != pstPktLast)
        {
            pstPktLast->pstNext = *pstPkt;
        }

        return;
    }
    PROXYD_SocketLinkSendToTunnel(aucPktBuf, uiPktLen+uiHeadLen, pstTopoInfo);
    return;
}

#if 0
#endif

int PROXYD_LoopReadDeal(int uiEpollFd, UINT uiFd)
{
    PROXYD_SERVER_TOPO_INFO_T *pstTopoInfo = NULL;
    pstTopoInfo = PROXYD_FindProxyTopoByFd(uiFd);
    if(NULL == pstTopoInfo)
    {
        CW_SendDebug("WARNING: NOT table value for event id() and sock(%d)", uiFd);
        return IPC_STATUS_FAIL;
    }
    switch(pstTopoInfo->ucLinkType)
    {
        case PROXYD_LINK_TUNNEL_SERVER:
            PROXYD_SocketTunnelAccept(uiEpollFd, pstTopoInfo);
            break;
        case PROXYD_LINK_TUNNEL_CLIENT:
            PROXY_SocketRead(uiEpollFd, pstTopoInfo->stIndex.uiSocketFd,
                             pstTopoInfo, &pstTopoInfo->stPktBuf,PROXYD_SocketTunnelRead,
                             PROXYD_LoopCloseDeal);
            //PROXYD_SocketTunnelRead(uiEpollFd, pstTopoInfo);
            break;
        case PROXYD_LINK_PROXY_SERVER:
            PROXYD_SocketPorxyAccept(uiEpollFd, pstTopoInfo);
            break;
        case PROXYD_LINK_PROXY_CLIENT:
            PROXYD_SocketLinkRead(uiEpollFd, pstTopoInfo);
            break;
        default:
            CW_SendErr("Link type error! type=%d sock=%d", pstTopoInfo->ucLinkType, uiFd);
            break;
    }
    return IPC_STATUS_OK;
}

int PROXYD_LoopCloseDeal(int uiEpollFd, UINT uiFd)
{
    PROXYD_SERVER_TOPO_INFO_T *pstTopoInfo = NULL;
    pstTopoInfo = PROXYD_FindProxyTopoByFd(uiFd);
    if(NULL == pstTopoInfo)
    {
        CW_SendDebug("WARNING: NOT table value for event id() and sock(%d)", uiFd);
        return IPC_STATUS_FAIL;
    }
    PROXYD_DelProxyTopo(pstTopoInfo);
    return IPC_STATUS_OK;
}

int PROXYD_CfgInit()
{
    memset(&g_stProxyIpHead, 0, sizeof(g_stProxyIpHead));
    memset(&g_stProxySocketHead, 0, sizeof(g_stProxySocketHead));
}
int g_uiEpollFd = 0;

static void free_all_quit(void)
{
    PROXYD_FreeProxyTopo();
    close(g_uiEpollFd);
    exit(0);
}

void signal_process(int sig){
	switch(sig) {
	case SIGTERM:
	case SIGKILL:
	case SIGSEGV:
	case SIGSTOP:
	case SIGABRT:
	case SIGQUIT:
	case SIGINT:
		free_all_quit();
	}
}
static void signal_setup(void)
{
	signal(SIGUSR1, signal_process);
	signal(SIGUSR2, signal_process);
	signal(SIGTERM, signal_process);
	signal(SIGKILL, signal_process);
//	signal(SIGSEGV, signal_process);
	signal(SIGSTOP, signal_process);
	signal(SIGABRT, signal_process);
	signal(SIGQUIT, signal_process);
	signal(SIGINT, signal_process);
}

int main()
{
    int uiManageFd;
    PROXYD_SERVER_TOPO_INFO_T stTmp;
    UINT uiIp = INADDR_ANY;
    g_uiEpollFd = SOCKET_CreateEpoll();
    PROXYD_CfgInit();

    uiManageFd = SOCKET_TcpServerCreate(uiIp ,PROXY_SERVERPORT);
    if(uiManageFd==-1)
    {
        CW_SendErr("SOCKET_TcpServerCreate, ip=%s, port=%d", inet_ntoa(*(struct in_addr*)&(uiIp)), PROXY_SERVERPORT);
        return -1;
    }
    memset(&stTmp, 0, sizeof(stTmp));
    stTmp.stIndex.uiSocketFd= uiManageFd;
    stTmp.stIndex.uiPort = PROXY_SERVERPORT;
    stTmp.stIndex.uiIp = uiIp;
    stTmp.ucLinkType = PROXYD_LINK_TUNNEL_SERVER;
    PROXYD_AddProxyTopo(&stTmp);

    SOCKET_AddEpoll(g_uiEpollFd, uiManageFd, EPOLLIN);
    signal_setup();
	timer_init();

    while(SOCKET_EpollLoop(g_uiEpollFd, PROXYD_LoopReadDeal, NULL, PROXYD_LoopCloseDeal));
    free_all_quit();

    return 0;
}

